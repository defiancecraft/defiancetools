package com.archeinteractive.defiancetools.database.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.UUID;

@Entity(value = "users", noClassnameStored = true)
public class User {

    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String uuid;
    @Indexed
    private String name;
    private String ip;
    private boolean vanished = false;
    @Embedded
    private ModerationHistory moderationHistory = new ModerationHistory();

    public User() {}

    public User(String uuid) {
        this.uuid = uuid;
    }

    public ObjectId getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isVanished() {
        return vanished;
    }

    public void setVanished(boolean vanished) {
        this.vanished = vanished;
    }

    public ModerationHistory getModerationHistory() {
        return moderationHistory;
    }
}
