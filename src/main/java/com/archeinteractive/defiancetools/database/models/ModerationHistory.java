package com.archeinteractive.defiancetools.database.models;

import org.mongodb.morphia.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

public class ModerationHistory {

    @Reference
    private List<ModerationEntry> entries = new ArrayList<>();
    @Reference
    private List<ModerationReport> reports = new ArrayList<>();
    @Reference
    private ModerationEntry latestBan = null;
    @Reference
    private ModerationEntry latestKick = null;
    @Reference
    private ModerationEntry latestMute = null;

    public ModerationHistory() {}

    public List<ModerationEntry> getEntries() {
        return entries;
    }

    public List<ModerationReport> getReports() {
        return reports;
    }

    public void addEntry(ModerationEntry entry) {
        entries.add(entry);

        switch (entry.getType()) {
            case BAN:
                latestBan = entry;
                break;
            case KICK:
                latestKick = entry;
                break;
            case MUTE:
                latestMute = entry;
                break;
        }

        entry.save();
    }

    public void addReport(ModerationReport report) {
        reports.add(report);
        report.save();
    }

    public boolean isBanned() {
        if (latestBan != null) {
            if (latestBan.getDuration() == -1 || (latestBan.getDate().getTime() + latestBan.getDuration()) > System.currentTimeMillis()) {
                return true;
            }
        }

        return false;
    }

    public long getRemainingTime() {
        if (latestBan != null) {
            long remaining = latestBan.getDate().getTime() + latestBan.getDuration() - System.currentTimeMillis();
            return remaining > 0 ? remaining : 0;
        }

        return 0;
    }

    public ModerationEntry getLatestBan() {
        return latestBan;
    }

    public ModerationEntry getLatestKick() {
        return latestKick;
    }

    public ModerationEntry getLatestMute() {
        return latestMute;
    }

    public void unban() {
        latestBan = null;
    }
}
