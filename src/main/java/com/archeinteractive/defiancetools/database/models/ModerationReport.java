package com.archeinteractive.defiancetools.database.models;

import com.archeinteractive.defiancetools.database.dao.ModerationReportDAO;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.Date;

@Entity(value = "users_moderation_history_reports", noClassnameStored = true)
public class ModerationReport {

    @Id
    private ObjectId id;
    private String reporter;
    private String reporterUuid;
    @Indexed
    private String name;
    @Indexed
    private String uuid;
    private String reason;
    private Date date;

    public ModerationReport() {}

    public ModerationReport(String reporter, String reporterUuid, String name, String uuid, String reason, Date date) {
        this.reporter = reporter;
        this.reporterUuid = reporterUuid;
        this.name = name;
        this.uuid = uuid;
        this.reason = reason;
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public String getReporter() {
        return reporter;
    }

    public String getReporterUuid() {
        return reporterUuid;
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getReason() {
        return reason;
    }

    public Date getDate() {
        return date;
    }

    public void save() {
        ModerationReportDAO.getInstance().save(this);
    }
}
