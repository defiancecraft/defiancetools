package com.archeinteractive.defiancetools.database.models;

public enum ModerationType {

    BAN,
    KICK,
    MUTE;

}
