package com.archeinteractive.defiancetools.database.models;

import com.archeinteractive.defiancetools.database.dao.ModerationEntryDAO;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.Date;

@Entity(value = "users_moderation_history_entries", noClassnameStored = true)
public class ModerationEntry {

    @Id
    private ObjectId id;
    private ModerationType type;
    private String reason;
    private Date date;
    private long duration;

    public ModerationEntry() {}

    public ModerationEntry(ModerationType type, String reason, Date date, long duration) {
        this.type = type;
        this.reason = reason;
        this.date = date;
        this.duration = duration;
    }

    public ObjectId getId() {
        return id;
    }

    public ModerationType getType() {
        return type;
    }

    public String getReason() {
        return reason;
    }

    public Date getDate() {
        return date;
    }

    public long getDuration() {
        return duration;
    }

    public void save() {
        ModerationEntryDAO.getInstance().save(this);
    }
}
