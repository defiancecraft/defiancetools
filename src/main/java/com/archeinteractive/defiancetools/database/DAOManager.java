package com.archeinteractive.defiancetools.database;

import com.archeinteractive.defiancetools.DefianceTools;
import com.archeinteractive.defiancetools.database.dao.ModerationEntryDAO;
import com.archeinteractive.defiancetools.database.dao.ModerationReportDAO;
import com.archeinteractive.defiancetools.database.dao.UserDAO;
import com.archeinteractive.defiancetools.database.models.ModerationEntry;
import com.archeinteractive.defiancetools.database.models.ModerationReport;
import com.archeinteractive.defiancetools.database.models.User;
import org.mongodb.morphia.Datastore;

import java.util.HashSet;

public class DAOManager {
    private static Datastore datastore;

    public DAOManager(ResourceManager resourceManager) {
        datastore = resourceManager.getDatastore(new HashSet<Class>(DefianceTools.getInstance().getDatabaseClasses()));
        ensureIndexes(datastore);
        registerDAO(datastore);
    }

    private void ensureIndexes(Datastore datastore) {
        datastore.ensureIndexes(User.class);
        datastore.ensureIndexes(ModerationEntry.class);
        datastore.ensureIndexes(ModerationReport.class);
    }

    private void registerDAO(Datastore datastore) {
        new UserDAO(datastore);
        new ModerationEntryDAO(datastore);
        new ModerationReportDAO(datastore);
    }
}
