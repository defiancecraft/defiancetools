package com.archeinteractive.defiancetools.database.dao;

import com.archeinteractive.defiancetools.database.models.User;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class UserDAO extends BasicDAO<User, ObjectId> {

    private static UserDAO instance;

    public UserDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static UserDAO getInstance() {
        return instance;
    }
}
