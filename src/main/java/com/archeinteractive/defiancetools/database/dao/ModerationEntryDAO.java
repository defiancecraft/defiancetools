package com.archeinteractive.defiancetools.database.dao;

import com.archeinteractive.defiancetools.database.models.ModerationEntry;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class ModerationEntryDAO extends BasicDAO<ModerationEntry, ObjectId> {

    private static ModerationEntryDAO instance;

    public ModerationEntryDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static ModerationEntryDAO getInstance() {
        return instance;
    }
}