package com.archeinteractive.defiancetools.database.dao;

import com.archeinteractive.defiancetools.database.models.ModerationReport;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class ModerationReportDAO extends BasicDAO<ModerationReport, ObjectId> {

    private static ModerationReportDAO instance;

    public ModerationReportDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static ModerationReportDAO getInstance() {
        return instance;
    }
}