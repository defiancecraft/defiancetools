package com.archeinteractive.defiancetools.database;

import com.archeinteractive.defiancetools.DefianceTools;
import com.archeinteractive.defiancetools.config.Settings;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.Set;

public class ResourceManager {
    private static ResourceManager instance;
    private MongoResource resource;

    public ResourceManager() {
        instance = this;

        if (resource == null) {
            resource = new MongoResource(getDatabaseObject(), DefianceTools.getInstance().getLogger());
        }

        new DAOManager(this);
    }

    private DatabaseObject getDatabaseObject() {
        Settings settings = DefianceTools.getInstance().getSettings();

        DatabaseObject databaseObject = new DatabaseObject(settings.getDatabase().getHost(), settings.getDatabase().getPort());

        if (settings.getDatabase().getUsername().equals("") == false && settings.getDatabase().getPassword().equals("") == false) {
            databaseObject.withUsername(settings.getDatabase().getUsername());
            databaseObject.withPassword(settings.getDatabase().getPassword());
        }

        if (settings.getDatabase().getDatabase().equals("") == false) {
            databaseObject.withDatabase(settings.getDatabase().getDatabase());
        }

        return databaseObject;
    }

    public Datastore getDatastore(Set<Class> databaseClasses) {
        Datastore datastore;

        datastore = new Morphia(databaseClasses).createDatastore(resource.mongoClient, resource.databaseObject.getDatabase());
        resource.authenticate(datastore);

        return datastore;
    }

    public Datastore getDatastore() {
        Datastore datastore = new Morphia().createDatastore(resource.mongoClient, resource.databaseObject.getDatabase());
        resource.authenticate(datastore);

        return datastore;
    }
}
