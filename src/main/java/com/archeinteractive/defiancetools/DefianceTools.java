package com.archeinteractive.defiancetools;

import com.archeinteractive.defiancetools.commands.ModerationCommands;
import com.archeinteractive.defiancetools.database.ResourceManager;
import com.archeinteractive.defiancetools.database.models.ModerationEntry;
import com.archeinteractive.defiancetools.database.models.User;
import com.archeinteractive.defiancetools.entity.PersistentUser;
import com.archeinteractive.defiancetools.listener.ConnectionListener;
import com.archeinteractive.defiancetools.listener.VanishListener;
import com.archeinteractive.defiancetools.util.JsonConfig;
import com.archeinteractive.defiancetools.util.VanishFactory;
import com.archeinteractive.defiancetools.config.Settings;
import com.archeinteractive.defiancetools.util.command.CommandListener;
import com.archeinteractive.defiancetools.util.command.CommandRegistry;
import com.archeinteractive.defiancetools.util.ui.MenuAPI;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefianceTools extends JavaPlugin {

    private static DefianceTools instance;
    private Settings settings = null;
    private ResourceManager resourceManager;
    private MenuAPI menuAPI;
    private Map<String, PersistentUser> users = new HashMap<>();
    private ModerationManager moderationManager;

    public void onEnable() {
        instance = this;

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        resourceManager = new ResourceManager();
        menuAPI = new MenuAPI(this);
        moderationManager = new ModerationManager();
        registerListeners();
        registerCommands();
        new VanishFactory(this);
    }

    public void onDisable() {
        settings.save(new File(getDataFolder(), "settings.json"));
    }

    public static DefianceTools getInstance() {
        return instance;
    }

    public Settings getSettings() {
        return settings;
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    public void registerListeners() {
        new ConnectionListener();
        new VanishListener();
    }

    public void registerCommands() {
        CommandListener.setup(this);
        CommandRegistry.registerPlayerCommand(this, "hmvanish", "defiancetools.vanish", ModerationCommands::vanish);
        CommandRegistry.registerUniversalCommand(this, "ban", "defiancetools.ban", ModerationCommands::ban);
        CommandRegistry.registerUniversalCommand(this, "tempban", "defiancetools.tempban", ModerationCommands::tempban);
        CommandRegistry.registerUniversalCommand(this, "unban", "defiancetools.unban", ModerationCommands::unban);
        CommandRegistry.registerPlayerCommand(this, "report", "defiancetools.report", ModerationCommands::report);
        CommandRegistry.registerPlayerCommand(this, "reportlog", "defiancetools.reportlog", ModerationCommands::reportLog);
        CommandRegistry.registerPlayerCommand(this, "reportlogs", "defiancetools.reportlogs", ModerationCommands::timedReportLog);
    }

    @Override
    public List<Class<?>> getDatabaseClasses() {
        List<Class<?>> list = new ArrayList<>();
        list.add(User.class);
        list.add(ModerationEntry.class);
        return list;
    }

    public Map<String, PersistentUser> getUsers() {
        return users;
    }
}
