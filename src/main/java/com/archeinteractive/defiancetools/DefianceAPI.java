package com.archeinteractive.defiancetools;

import com.archeinteractive.defiancetools.database.dao.ModerationEntryDAO;
import com.archeinteractive.defiancetools.database.dao.ModerationReportDAO;
import com.archeinteractive.defiancetools.database.dao.UserDAO;
import com.archeinteractive.defiancetools.database.models.*;
import org.bukkit.entity.Player;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.Date;
import java.util.UUID;

public class DefianceAPI {

    /**
     * Initializes a network user instance for an online player.
     *
     * @param name the name of the user
     * @param uuid the uuid of the user
     * @param ip the ip of the user
     * @return Networkuser instance
     */
    public static User initUser(UUID uuid, String name, String ip) {
        User user = retrieveUser(uuid);

        if (user == null) {
            user = new User(uuid.toString());
        }

        updateUser(user, name, ip);

        return user;
    }

    /**
     * Updates the core data of a user.
     *
     * @param user
     * @param name
     * @param ip
     */
    public static void updateUser(User user, String name, String ip) {
        boolean updated = false;
        if (user.getName() == null || user.getName().equalsIgnoreCase(name) == false) {
            user.setName(name);
            updated = true;
        }

        if (user.getIp() == null || user.getIp().equalsIgnoreCase(ip) == false) {
            user.setIp(ip);
            updated = true;
        }

        if (updated) {
            UserDAO.getInstance().save(user);
        }
    }

    /**
     * Retrieves a profile matching the specified uuid.
     *
     * @param uuid
     * @return
     */
    public static User retrieveUser(UUID uuid) {
        return UserDAO.getInstance().findOne("uuid", uuid.toString());
    }

    /**
     * Retrieves a profile matching the specified name.
     *
     * @param name
     * @return
     */
    public static User retrieveUser(String name) {
        for (User user : UserDAO.getInstance().createQuery().field("name").containsIgnoreCase(name).fetch()) {
            if (user.getName().equalsIgnoreCase(name)) {
                return user;
            }
        }

        return null;
    }

    public static void banUser(User user, String reason) {
        ModerationHistory history = user.getModerationHistory();
        ModerationEntry entry = new ModerationEntry(ModerationType.BAN, reason, new Date(System.currentTimeMillis()), -1);

        history.addEntry(entry);

        UpdateOperations<User> op = UserDAO.getInstance().createUpdateOperations();
        op.set("moderationHistory", history);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("id").equal(user.getId()), op);
    }

    public static void tempbanUser(User user, String reason, long duration) {
        ModerationHistory history = user.getModerationHistory();
        ModerationEntry entry = new ModerationEntry(ModerationType.BAN, reason, new Date(System.currentTimeMillis()), duration);

        history.addEntry(entry);

        UpdateOperations<User> op = UserDAO.getInstance().createUpdateOperations();
        op.set("moderationHistory", history);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("id").equal(user.getId()), op);
    }

    public static void unbanUser(User user) {
        ModerationHistory history = user.getModerationHistory();
        history.unban();

        UpdateOperations<User> op = UserDAO.getInstance().createUpdateOperations();
        op.set("moderationHistory", history);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("id").equal(user.getId()), op);
    }

    public static void reportUser(User user, ModerationReport report) {
        ModerationHistory history = user.getModerationHistory();

        history.addReport(report);

        UpdateOperations<User> op = UserDAO.getInstance().createUpdateOperations();
        op.set("moderationHistory", history);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("id").equal(user.getId()), op);
    }

    public static Iterable<ModerationReport> retrieveModerationReportsAfter(Date date) {
        ModerationReportDAO dao = ModerationReportDAO.getInstance();
        Query<ModerationReport> query = dao.createQuery().field("date").greaterThanOrEq(date);
        return dao.find(query).fetch();
    }

    public static Iterable<ModerationReport> retrieveModerationReportsForPlayer(String name) {
        Query<ModerationReport> query = ModerationReportDAO.getInstance().createQuery().field("name").containsIgnoreCase(name);
        return ModerationReportDAO.getInstance().find(query).fetch();
    }

    public static Iterable<ModerationReport> retrieveModerationReportsForPlayer(Player player) {
        Query<ModerationReport> query = ModerationReportDAO.getInstance().createQuery().field("uuid").containsIgnoreCase(player.getUniqueId().toString());
        return ModerationReportDAO.getInstance().find(query).fetch();
    }

}
