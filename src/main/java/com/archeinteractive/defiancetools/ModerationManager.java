package com.archeinteractive.defiancetools;

import com.archeinteractive.defiancetools.database.models.ModerationReport;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ModerationManager {

    private static ModerationManager instance;
    private Map<ModerationReport, List<String>> recentReports = new HashMap<>();

    public ModerationManager() {
        instance = this;
        retrieveReports();
        scheduleReportUpdates();
    }

    private void retrieveReports() {
        for (ModerationReport report : DefianceAPI.retrieveModerationReportsAfter(new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(24)))) {
            recentReports.put(report, new ArrayList<>());
        }
    }

    private void scheduleReportUpdates() {
        new BukkitRunnable() {
            @Override
            public void run() {
                List<ModerationReport> reports = new ArrayList<ModerationReport>(recentReports.keySet());
                for (ModerationReport report : reports) {
                    if (System.currentTimeMillis() - report.getDate().getTime() > TimeUnit.HOURS.toMillis(24)) {
                        recentReports.remove(report);
                    }
                }
            }
        }.runTaskTimer(DefianceTools.getInstance(), 20 * 60 * 5, 20 * 60 * 5);
    }

    public void informPlayer(Player player) {
        int reports = 0;
        List<String> players = new ArrayList<>();
        for (Map.Entry<ModerationReport, List<String>> entry : recentReports.entrySet()) {
            if (entry.getValue().contains(player.getName()) == false) {
                reports++;
                players.add(entry.getKey().getName().toLowerCase());

                entry.getValue().add(player.getName());
            }
        }

        if (reports > 0) {
            player.sendMessage(ChatColor.RED + "You have " + reports + " unread reports for " + players.size() + " players!");
        }
    }

    public void informPlayers(ModerationReport report) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission("defiancetools.showrecentreports")) {
                player.sendMessage(ChatColor.RED + "Player Report: " + ChatColor.GOLD + report.getName());
                player.sendMessage(ChatColor.RED + "Submitted By: " + ChatColor.GOLD + report.getReporter());
                if (report.getReason() != null) {
                    player.sendMessage(ChatColor.RED + "Reason: " + ChatColor.GOLD + report.getReason());
                }
                player.sendMessage(ChatColor.RED + "Date Submitted: " + ChatColor.GOLD + report.getDate().toString());
                player.sendMessage("\n");

                recentReports.get(report).add(player.getName());
            }
        }
    }

    public void addReport(ModerationReport report) {
        recentReports.put(report, new ArrayList<>());
        informPlayers(report);
    }

    public static ModerationManager getInstance() {
        return instance;
    }
}
