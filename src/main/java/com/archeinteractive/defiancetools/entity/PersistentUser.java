package com.archeinteractive.defiancetools.entity;

import java.util.UUID;

public class PersistentUser {

    private String name;
    private UUID uuid;
    private boolean vanished;

    public PersistentUser(String name, UUID uuid, boolean vanished) {
        this.name = name;
        this.uuid = uuid;
        this.vanished = vanished;
    }

    public boolean isVanished() {
        return vanished;
    }

    public void setVanished(boolean vanished) {
        this.vanished = vanished;
    }

    public String getName() {
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }
}
