package com.archeinteractive.defiancetools.listener;

import com.archeinteractive.defiancetools.DefianceTools;
import com.archeinteractive.defiancetools.config.components.CWorld;
import com.archeinteractive.defiancetools.entity.PersistentUser;
import com.archeinteractive.defiancetools.util.VanishFactory;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.*;

public class VanishListener implements Listener {

    public VanishListener() {
        Bukkit.getPluginManager().registerEvents(this, DefianceTools.getInstance());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemPickup(PlayerPickupItemEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemDrop(PlayerDropItemEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        Entity entityTarget = event.getEntity();

        if (entityTarget instanceof Player) {
            Player p = (Player) entityTarget;
            PersistentUser user = DefianceTools.getInstance().getUsers().get(p.getName());

            if (user.isVanished()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteract(PlayerInteractEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteractEntity(PlayerInteractEntityEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());
        if (user.isVanished()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof Player) {
            Player player = (Player) entity;
            PersistentUser user = DefianceTools.getInstance().getUsers().get(player.getName());

            if (user.isVanished()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityTarget(EntityTargetLivingEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            return;
        }

        if (event.getTarget() instanceof Player) {
            Player player = (Player) event.getTarget();
            PersistentUser user = DefianceTools.getInstance().getUsers().get(player.getName());

            if (user.isVanished()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (VanishFactory.getInstance().hasPlayer(event.getPlayer())) {
            World world = event.getTo().getWorld();
            for (String w : DefianceTools.getInstance().getSettings().getWorlds().keySet()) {
                if (world.getName().equalsIgnoreCase(w)) {
                    CWorld cWorld = DefianceTools.getInstance().getSettings().getWorlds().get(w);
                    if (cWorld.isVanishAllowed() == false) {
                        VanishFactory.getInstance().vanish(event.getPlayer(), false);
                    }
                }
            }
        }
    }

}
