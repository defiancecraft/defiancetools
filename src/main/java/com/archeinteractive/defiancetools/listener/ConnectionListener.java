package com.archeinteractive.defiancetools.listener;

import com.archeinteractive.defiancetools.DefianceAPI;
import com.archeinteractive.defiancetools.DefianceTools;
import com.archeinteractive.defiancetools.ModerationManager;
import com.archeinteractive.defiancetools.database.dao.UserDAO;
import com.archeinteractive.defiancetools.database.models.User;
import com.archeinteractive.defiancetools.entity.PersistentUser;
import com.archeinteractive.defiancetools.util.TimeUtil;
import com.archeinteractive.defiancetools.util.VanishFactory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    public ConnectionListener() {
        Bukkit.getPluginManager().registerEvents(this, DefianceTools.getInstance());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
    	
    	// Sanity check.
    	if (event.getUniqueId() == null) {
    		event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
    		event.setKickMessage(ChatColor.RED + "An internal server error occurred! Sorry, try again later.");
    		System.out.println("===========================\n\n" +
			    				"For whatever reason, " + event.getName() + " (IP " + event.getAddress().getHostAddress() + ")\n" +
			    				"was kicked as their UUID was null... I don't know either.\n\n" +
			    				"===========================");
    		return;
    	}
    	
        User user = DefianceAPI.initUser(event.getUniqueId(), event.getName(), event.getAddress().getHostAddress());

        if (user.getModerationHistory().isBanned()) {
            String reason = user.getModerationHistory().getLatestBan().getReason();
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_BANNED);
            if (user.getModerationHistory().getLatestBan().getDuration() == -1) {
                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned from this network for: &6" + reason
                        + "\n&6Appeal at: &cwww.defiancecraft.com"));
            } else {
                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are temporarily banned from the network!"
                        + "\n&6Reason: &c" + reason
                        + "\n&6You will be unbanned in &c" + TimeUtil.convertToFormattedTime(user.getModerationHistory().getRemainingTime())
                        + "\n&6Appeal at: &cwww.defiancecraft.com"));
            }
        }

        DefianceTools.getInstance().getUsers().put(event.getName(), new PersistentUser(event.getName(), event.getUniqueId(), user.isVanished()));
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());

        if (user.isVanished()) {
            VanishFactory.getInstance().vanish(event.getPlayer(), true);
        } else {
            VanishFactory.getInstance().vanish(event.getPlayer(), false);
        }

        for (String p : VanishFactory.getInstance().getGhosts()) {
            PersistentUser u = DefianceTools.getInstance().getUsers().get(p);
            if (u.getName().equalsIgnoreCase(user.getName()) == false) {
                if (u.isVanished()) {
                    Player player = Bukkit.getPlayer(u.getUuid());
                    if (player != null) {
                        event.getPlayer().hidePlayer(player);
                    }
                }
            }
        }

        if (event.getPlayer().hasPermission("defiancetools.showrecentreports")) {
            ModerationManager.getInstance().informPlayer(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        User user = DefianceAPI.retrieveUser(event.getPlayer().getUniqueId());
        PersistentUser u = DefianceTools.getInstance().getUsers().get(event.getPlayer().getName());

        if (VanishFactory.getInstance().hasPlayer(event.getPlayer())) {
            VanishFactory.getInstance().vanish(event.getPlayer(), false);
        }

        if (user != null && u != null) {
            boolean dirty = false;
            if (user.isVanished() != u.isVanished()) {
                user.setVanished(u.isVanished());
                dirty = true;
            }

            if (dirty) {
                UserDAO.getInstance().save(user);
            }
        }
    }

}
