package com.archeinteractive.defiancetools.commands;

import com.archeinteractive.defiancetools.DefianceAPI;
import com.archeinteractive.defiancetools.DefianceTools;
import com.archeinteractive.defiancetools.ModerationManager;
import com.archeinteractive.defiancetools.database.models.ModerationReport;
import com.archeinteractive.defiancetools.database.models.User;
import com.archeinteractive.defiancetools.entity.PersistentUser;
import com.archeinteractive.defiancetools.menus.PlayerReportHistory;
import com.archeinteractive.defiancetools.menus.TimedHistoryReport;
import com.archeinteractive.defiancetools.util.TimeUtil;
import com.archeinteractive.defiancetools.util.VanishFactory;
import com.archeinteractive.defiancetools.util.ui.PaginatedMenu;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class ModerationCommands {

    public static boolean vanish(Player sender, String[] args) {
        PersistentUser user = DefianceTools.getInstance().getUsers().get(sender.getName());
        sender.sendMessage(ChatColor.GREEN + "You will " + (user.isVanished() ? "unvanish" : "vanish") + " in " + ChatColor.GOLD + DefianceTools.getInstance().getSettings().getVanishActivationDelay() + ChatColor.GREEN + " seconds!");

        user.setVanished(!user.isVanished());

        // Schedule vanish using activation delay.
        new BukkitRunnable() {
            @Override
            public void run() {
                VanishFactory.getInstance().vanish(sender, user.isVanished());
            }
        }.runTaskLater(DefianceTools.getInstance(), DefianceTools.getInstance().getSettings().getVanishActivationDelay() * 20);
        return true;
    }

    public static boolean ban(CommandSender sender, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/ban <player> <reason>");
            return true;
        }

        String player = args[0];
        String reason = args[1];;

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DefianceAPI.retrieveUser(target.getUniqueId()) : DefianceAPI.retrieveUser(player);

        if (user == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUnable to find user in the database. Is the name correct?"));
            return true;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned, visit &6defiancecraft.com&c to appeal this ban."
                    + "\n&6Reason: &c" + reason));
        }

        DefianceAPI.banUser(user, reason);
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l»&6&l» &e&l" + player +
                " &cwas banned!"));
        return true;
    }

    public static boolean tempban(CommandSender sender, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/tempban <player> <#s|#m|#h|#d> <reason>");
            return true;
        }

        String player = args[0];
        String time = args[1];
        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DefianceAPI.retrieveUser(target.getUniqueId()) : DefianceAPI.retrieveUser(player);

        if (user == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUnable to find user in the database. Is the name correct?"));
            return true;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned, visit &6defiancecraft.com&c to appeal this ban."
                    + "\n&6Reason: &c" + reason));
        }

        long duration = 0;
        try {
            duration = TimeUtil.getTime(time);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/tempban <player> <#s|#m|#h|#d> <reason>");
            return true;
        }

        DefianceAPI.tempbanUser(user, reason, duration);
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l»&6&l» &e&l" + player +
                " &cwas tempbanned!"));
        return true;
    }

    public static boolean unban(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/unban <player>");
            return true;
        }

        String player = args[0];
        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DefianceAPI.retrieveUser(target.getUniqueId()) : DefianceAPI.retrieveUser(player);

        if (user == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUnable to find user in the database. Is the name correct?"));
            return true;
        }

        DefianceAPI.unbanUser(user);

        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l»&6&l» &e&l" + player +
                " &cwas unbanned!"));
        return true;
    }

    public static boolean report(Player sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/report <player> [reason]");
            return true;
        }

        String player = args[0];
        String reason = null;

        if (args.length >= 2) {
            reason = args[1];
            if (args.length > 2) {
                for (int i = 2; i < args.length; i++) {
                    reason += " " + args[i];
                }
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DefianceAPI.retrieveUser(target.getUniqueId()) : DefianceAPI.retrieveUser(player);

        if (user == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUnable to find user in the database. Is the name correct?"));
            return true;
        }

        ModerationReport report = new ModerationReport(sender.getName(), sender.getUniqueId().toString(), player, user.getUuid(), reason, new Date(System.currentTimeMillis()));
        DefianceAPI.reportUser(user, report);
        ModerationManager.getInstance().addReport(report);
        sender.sendMessage(ChatColor.GREEN + "Your report was submitted successfully!");
        return true;
    }

    public static boolean reportLog(Player sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/reportlog <player>");
            return true;
        }

        String player = args[0];
        Player target = Bukkit.getPlayer(player);
        Iterable<ModerationReport> reports = target == null ? DefianceAPI.retrieveModerationReportsForPlayer(player) : DefianceAPI.retrieveModerationReportsForPlayer(target);

        if (reports == null || reports.iterator().hasNext() == false) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThere are no reports for this player."));
            return true;
        }

        List<ModerationReport> reportList = new ArrayList<>();
        Iterator iterator = reports.iterator();
        while (iterator.hasNext()) {
            reportList.add((ModerationReport)iterator.next());
        }

        new PlayerReportHistory(sender.getUniqueId(), player, reportList).openMenu(0, sender);
        return true;
    }

    public static boolean timedReportLog(Player sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GRAY + "/reportlogs <# of days>");
            return true;
        }

        String time = args[0];
        int days = 0;

        try {
            days = Integer.parseInt(time);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Your input contains invalid characters. You must use numbers only.");
            return true;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Iterable<ModerationReport> reports = DefianceAPI.retrieveModerationReportsAfter(calendar.getTime());

        if (reports == null || reports.iterator().hasNext() == false) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThere were no reports to be found."));
            return true;
        }

        List<ModerationReport> reportList = new ArrayList<>();
        Iterator iterator = reports.iterator();
        while (iterator.hasNext()) {
            reportList.add((ModerationReport)iterator.next());
        }

        new TimedHistoryReport(sender.getUniqueId(), days, reportList).openMenu(0, sender);
        return true;
    }
}
