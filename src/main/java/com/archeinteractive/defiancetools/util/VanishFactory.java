package com.archeinteractive.defiancetools.util;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class VanishFactory {
    /**
     * Team of ghosts and people who can see ghosts.
     */
    private static VanishFactory instance;

    // Players that are actually ghosts
    private Set<String> ghosts = new HashSet<String>();

    public VanishFactory(Plugin plugin) {
        instance = this;
    }

    /**
     * Determine if the current player is tracked by this ghost manager, or is a ghost.
     * @param player - the player to check.
     * @return TRUE if it is, FALSE otherwise.
     */
    public boolean hasPlayer(Player player) {
        return ghosts.contains(player.getName().toLowerCase());
    }

    public void vanish(Player player, boolean enabled) {
        if (enabled) {
            ghosts.add(player.getName());
            player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 15));
            player.setAllowFlight(true);
            player.setFlying(true);

            for (Player p : Bukkit.getOnlinePlayers()) {
                if (p != player) {
                    p.hidePlayer(player);
                }
            }
        } else if (!enabled) {
            ghosts.remove(player.getName());
            player.removePotionEffect(PotionEffectType.INVISIBILITY);

            if (!player.isOp()) {
                player.setAllowFlight(false);
            }

            if (player.getGameMode() != GameMode.CREATIVE) {
                player.setFlying(false);
            }

            for (Player p : Bukkit.getOnlinePlayers()) {
                if (p != player) {
                    p.showPlayer(player);
                }
            }
        }
    }

    public Set<String> getGhosts() {
        return ghosts;
    }

    public static VanishFactory getInstance() {
        return instance;
    }
}
