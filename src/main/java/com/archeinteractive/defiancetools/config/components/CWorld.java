package com.archeinteractive.defiancetools.config.components;

public class CWorld {

    private boolean vanishAllowed = true;

    public boolean isVanishAllowed() {
        return vanishAllowed;
    }
}
