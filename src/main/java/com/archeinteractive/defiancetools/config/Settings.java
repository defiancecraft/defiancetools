package com.archeinteractive.defiancetools.config;

import com.archeinteractive.defiancetools.util.CaseInsensitiveHashMap;
import com.archeinteractive.defiancetools.util.JsonConfig;
import com.archeinteractive.defiancetools.config.components.Database;
import com.archeinteractive.defiancetools.config.components.CWorld;

import java.util.Map;

public class Settings extends JsonConfig {

    private Map<String, CWorld> worlds = new CaseInsensitiveHashMap<CWorld>(){{
        put("world", new CWorld());
    }};
    private int vanishActivationDelay = 5;
    private Database database = new Database();

    public Map<String, CWorld> getWorlds() {
        return worlds;
    }

    public int getVanishActivationDelay() {
        return vanishActivationDelay;
    }

    public Database getDatabase() {
        return database;
    }
}
