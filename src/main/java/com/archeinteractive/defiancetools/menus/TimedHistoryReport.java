package com.archeinteractive.defiancetools.menus;

import com.archeinteractive.defiancetools.database.models.ModerationReport;
import com.archeinteractive.defiancetools.util.ui.MenuItem;
import com.archeinteractive.defiancetools.util.ui.PaginatedMenu;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.util.*;

public class TimedHistoryReport extends PaginatedMenu {

    private List<ModerationReport> reports;
    private List<ItemStack> itemStacks = new ArrayList<>();
    private UUID viewer;
    private int page = 0;
    private MenuItem next;
    private MenuItem previous;

    public TimedHistoryReport(UUID viewer, int days, List<ModerationReport> reports) {
        super("Report History Within " + days + " Days", 6, (int)Math.ceil(reports.size() / 54) + 1);
        this.viewer = viewer;
        this.reports = reports;
        prepare();
        build();
    }

    public void prepare() {
        Collections.sort(reports, (m1, m2) -> m2.getDate().compareTo(m1.getDate()));

        for (ModerationReport report : reports) {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(report.getReporter());
            meta.setDisplayName(ChatColor.GOLD + "Submitted By: " + ChatColor.RED + report.getReporter());
            meta.setLore(new ArrayList<String>() {{
                add(ChatColor.GOLD + "Reason: " + ChatColor.RED + (report.getReason() == null ? "n/a" : (report.getReason().length() > 30 ? report.getReason().substring(0, 30) : report.getReason())));
                add(ChatColor.GOLD + "Submitted On: " + ChatColor.RED + report.getDate().toString());
            }});
            item.setItemMeta(meta);
            itemStacks.add(item);
        }

        next = new MenuItem("Next Page", new MaterialData(Material.WOOL, DyeColor.GREEN.getWoolData())) {
            @Override
            public void onClick(Player player) {
                next();
            }
        };

        previous = new MenuItem("Previous Page", new MaterialData(Material.WOOL, DyeColor.RED.getWoolData())) {
            @Override
            public void onClick(Player player) {
                previous();
            }
        };
    }

    public void build() {
        Iterator<ItemStack> iterator = itemStacks.iterator();
        for (int i = 0; i < this.getPageCount(); i++) {
            Inventory inventory = this.getInventory(i);
            for (int j = 0; j < 54; j++) {
                if ((j >= 6 && j <= 8) || (j >= 15 && j <= 17)) {
                    if (j == 7) {
                        this.addMenuItem(i, previous, j);
                    } else if (j == 8) {
                        this.addMenuItem(i, next, j);
                    } else {
                        ItemStack pane =  new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) DyeColor.LIGHT_BLUE.getData());
                        ItemMeta meta = pane.getItemMeta();
                        meta.setDisplayName("-.-.-");
                        pane.setItemMeta(meta);
                        inventory.setItem(j, pane);
                    }
                } else {
                    if (iterator.hasNext()) {
                        inventory.setItem(j, iterator.next());
                    } else {
                        if (j > 17) {
                            return;
                        }
                    }
                }
            }
        }
    }

    public void next() {
        if (page >= this.getPageCount() - 1) {
            page = 0;
        } else {
            page += 1;
        }

        open();
    }

    public void previous() {
        if (page <= 0) {
            page = this.getPageCount() - 1;
        } else {
            page -= 1;
        }

        open();
    }

    public void open() {
        Player player = Bukkit.getPlayer(viewer);
        this.openMenu(page, player);
    }

}
